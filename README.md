API Queuing Service
===================

There are 3 different external APIs that our service has to interface with. Each of the
APIs accepts a query-parameter ‘?q=’ that can accept multiple queries split using a
comma delimiter. If the same value is present multiple times in the query, the
response will only contain it once. Each of the APIs provides requests and responses
in their own unique manner as shown below. Expect all APIs to always return 200
OK responses with well-formed input, or 503 UNAVAILABLE when they are not
running.

### Scenario 1
Provide API method that that accepts a collection of API requests. Each API
request object consists of an API identifier and the query for that API. Upon calling,
the different calls should be forwarded to the individual APIs. Only upon receiving all
responses should the complete set of responses be returned to the caller.

### Scenario 2
To prevent overloading the APIs with query calls we would like to queue calls per
API endpoint. All incoming requests for each individual API should be kept in a
queue and be forwarded to the APIs as soon as a cap of 5 calls for an individual API
is reached.

### Scenario 3
Our current implementation has 1 major downside; the caller will not receive a
response to its requests if the queue cap for a specific service is not reached. To solve
this, we want the service queues to also be sent out every 5 seconds. In case of the cap
being reached within these 5 seconds, the scheduler should be reset to the initial duration.


## Implementation detail
Solution is made with akka-streams. All scenarios are implemented on top of in-memory stub api methods.
Only as an example HTTP approach is demonstrated in ms.konovalov.apiqueuing.HttpApi. Nevertheless HTTP 
implementation is quite straightforward and  was not completed due to lack of time.

Sbt can be used to build the project
```bash
% sbt package test
```
