package ms.konovalov.apiqueuing

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.Uri.Path
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import ms.konovalov.apiqueuing.ProductType.ProductType
import ms.konovalov.apiqueuing.StatusType.StatusType
import spray.json.{DefaultJsonProtocol, JsValue, RootJsonReader, deserializationError}

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success, Try}

/**
  * Trait encapsulates common http logic
  *
  * @inheritdoc
  */
trait HttpApi[ID, R, Req <: ApiRequest[ID], Res <: ApiResponse[R]] extends Api[ID, R, Req, Res] {

  implicit val system: ActorSystem
  implicit val mat: ActorMaterializer
  implicit val ec: ExecutionContext
  val baseUrl: String

  /**
    * Generate url string from ids
    *
    * @param input list of identifiers
    * @return url suffix
    */
  def idToUrl(input: Seq[ID]): String

  /*
   * Generate url string from wrapped request
   */
  private def requestToUrl(input: Seq[Req]): String = idToUrl(input.map(_.param))

  /**
    * @inheritdoc
    */
  override def call(ids: Seq[ID]): Future[Seq[R]] =
    Http().singleRequest(HttpRequest(uri = baseUrl + idToUrl(ids))).flatMap(httpResponse =>
      deserializeFunction(httpResponse).flatMap {
        case Success(response) => Future.successful(response.map(_.result))
        case Failure(e) => Future.failed(e)
      })

  /**
    * @inheritdoc
    */
  override def tryCall(ids: Seq[Req]): Future[Try[Seq[Res]]] =
    Http().singleRequest(HttpRequest(uri = baseUrl + requestToUrl(ids))).flatMap(deserializeFunction)

  /**
    * @inheritdoc
    */
  override def flowCall()(implicit ec: ExecutionContext): Flow[Seq[RequestContainer[Req, Res]], Seq[Promise[Res]], NotUsed] = {

    def pathToRequest(path: String) = HttpRequest(uri = Uri.Empty.withPath(Path(path)))

    val requestFlow = Flow[Seq[RequestContainer[Req, Res]]]
      .map(request => {
        (pathToRequest(requestToUrl(request.map(_.request))),
          request.map(_.result))
      })

    val connectionFlow = Http().cachedHostConnectionPool[Seq[Promise[Res]]](baseUrl)

    val resultFlow = Flow[(Try[HttpResponse], Seq[Promise[Res]])]
      .map {
        case (Success(httpResponse), promises) =>
          deserializeFunction(httpResponse).map {
            case Success(response) =>
              response.zip(promises).map { case (result, p) => p.success(result) }
            case Failure(e) =>
              promises.foreach(p => p.failure(e))
          }
          promises
        case (Failure(e), promises) =>
          promises.foreach(_.failure(e))
          promises
      }

    requestFlow via connectionFlow via resultFlow
  }

  /**
    * Function being used for http response deserialization
    *
    * @param httpResponse response
    * @return deserialized value wrapped in Try and Future
    */
  def deserializeFunction(httpResponse: HttpResponse): Future[Try[Seq[Res]]]
}

/**
  * Http client Api implementation for Track Api
  */
class TrackApiHttp(val baseUrl: String)(implicit val system: ActorSystem, val mat: ActorMaterializer, val ec: ExecutionContext)
  extends HttpApi[Int, StatusType, TrackRequest, TrackResponse] with TrackApi with ApiSerialization {

  override def idToUrl(input: Seq[Int]) = s"/track?q=${input.map(_.toString).mkString(",")}"

  override def deserializeFunction(httpResponse: HttpResponse): Future[Try[Seq[TrackResponse]]] = deserialize[Seq[TrackResponse]](httpResponse)
}

/**
  * Http client implementation for Shipment Api
  */
class ShipmentApiHttp(val baseUrl: String)(implicit val system: ActorSystem, val mat: ActorMaterializer, val ec: ExecutionContext)
  extends HttpApi[Int, Seq[ProductType], ShipmentRequest, ShipmentResponse] with ShipmentApi with ApiSerialization {

  override def idToUrl(input: Seq[Int]) = s"/shipments?q=${input.map(_.toString).mkString(",")}"

  override def deserializeFunction(httpResponse: HttpResponse): Future[Try[Seq[ShipmentResponse]]] = deserialize[Seq[ShipmentResponse]](httpResponse)
}

/**
  * Http client implementation for Pricing Api
  */
class PricingApiHttp(val baseUrl: String)(implicit val system: ActorSystem, val mat: ActorMaterializer, val ec: ExecutionContext)
  extends HttpApi[String, Float, PricingRequest, PricingResponse] with PricingApi with ApiSerialization {

  override def idToUrl(input: Seq[String]) = s"/shipments?q=${input.mkString(",")}"

  override def deserializeFunction(httpResponse: HttpResponse): Future[Try[Seq[PricingResponse]]] = deserialize[Seq[PricingResponse]](httpResponse)
}

/**
  * Exception in case of unexpected http status
  *
  * @param status http status
  */
case class UnexpectedStatusCode(status: StatusCode) extends Exception

/**
  * Json protocol
  */
trait ApiSerialization extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val trackFormat: RootJsonReader[Seq[TrackResponse]] = new RootJsonReader[Seq[TrackResponse]] {
    override def read(json: JsValue): Seq[TrackResponse] =
      mapFormat[String, String].read(json).values.toSeq.map(str =>
        StatusType.fromString(str).map(TrackResponse) match {
          case Some(response) => response
          case None => deserializationError("Cannot deserialize Track Response")
        })
  }

  implicit val shipmentFormat: RootJsonReader[Seq[ShipmentResponse]] = new RootJsonReader[Seq[ShipmentResponse]] {
    override def read(json: JsValue): Seq[ShipmentResponse] =
      mapFormat[String, Seq[String]].read(json).values.toSeq.map(str =>
        str.map(ProductType.fromString).map {
          case Some(response) => response
          case None => deserializationError("Cannot deserialize Track Response")
        }).map(ShipmentResponse)
  }

  implicit val pricingFormat: RootJsonReader[Seq[PricingResponse]] = new RootJsonReader[Seq[PricingResponse]] {
    override def read(json: JsValue): Seq[PricingResponse] =
      mapFormat[String, String].read(json).values.toSeq.map(fl => PricingResponse(fl.toFloat))
  }
}


