package ms.konovalov.apiqueuing

import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Keep, Sink, Source, SourceQueueWithComplete}

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.language.postfixOps
import scala.reflect.ClassTag

/**
  * Service that provides client methods
  * @param pricingApi pricing api implementation
  * @param shipmentApi shipment api implementation
  * @param trackApi tracking api implementation
  */
class Service(pricingApi: PricingApi = new PricingApiStub,
              shipmentApi: ShipmentApi = new ShipmentApiStub,
              trackApi: TrackApi = new TrackApiStub)
             (implicit system: ActorSystem,
              materializer: ActorMaterializer,
              ec: ExecutionContext) {

  private val groupSize = 5
  private val apisCount = 3
  private val timeout = 5 seconds

  /**
    * Simple direct call - for each item in request list makes single API call
    * @param calls list of api requests to call
    * @return list of results wrapped in future
    */
  def directCall(calls: Seq[ApiRequest[_]]): Future[Seq[ApiResponse[_]]] = {
    Future.sequence(
      calls.map(call => makeCalls(call))
    )
  }

  /*
   * Determine type of api to call
   */
  private def makeCalls(request: ApiRequest[_]): Future[ApiResponse[_]] = request match {
    case r: PricingRequest => makeSingleApiCall(pricingApi, r, PricingResponse.apply)
    case r: TrackRequest => makeSingleApiCall(trackApi, r, TrackResponse.apply)
    case r: ShipmentRequest => makeSingleApiCall(shipmentApi, r, ShipmentResponse.apply)
  }

  /*
   * Call direct API with provided client and wrap result
   */
  private def makeSingleApiCall[ID, Req <: ApiRequest[ID], R, Res <: ApiResponse[R]]
              (api: Api[ID, R, Req, Res], request: Req, convertResponse: R => Res) =
    api.call(Seq(request.param)).map(result =>
      result.map(res => convertResponse(res)).head
    )

  /* Source queue to push events from outside */
  private val source = Source.queue[RequestContainer[_, _]](groupSize, OverflowStrategy.backpressure)

  /* Graph that collects events into batches by 5 */
  private val batchGraph = Sink.fromGraph(GraphDSL.create() { implicit builder =>
    import GraphDSL.Implicits._

    val bcast = builder.add(Broadcast[RequestContainer[_, _]](apisCount))

    val shipmentFlow = describeApiFlow[ShipmentRequest, ShipmentResponse]
    val pricingFlow = describeApiFlow[PricingRequest, PricingResponse]
    val trackFlow = describeApiFlow[TrackRequest, TrackResponse]

    bcast ~> shipmentFlow ~> shipmentApi.flowCall() ~> Sink.ignore
    bcast ~> pricingFlow  ~> pricingApi.flowCall()  ~> Sink.ignore
    bcast ~> trackFlow    ~> trackApi.flowCall()    ~> Sink.ignore
    SinkShape(bcast.in)
  })

  /* Graph that collects events into batches by 5 or with timeout */
  private val batchWithTimeoutGraph = Sink.fromGraph(GraphDSL.create() { implicit builder =>
    import GraphDSL.Implicits._

    val bcast = builder.add(Broadcast[RequestContainer[_, _]](apisCount))

    val shipmentFlow = describeApiFlowWithTimeout[ShipmentRequest, ShipmentResponse]
    val pricingFlow = describeApiFlowWithTimeout[PricingRequest, PricingResponse]
    val trackFlow = describeApiFlowWithTimeout[TrackRequest, TrackResponse]

    bcast ~> shipmentFlow ~> shipmentApi.flowCall() ~> Sink.ignore
    bcast ~> pricingFlow  ~> pricingApi.flowCall()  ~> Sink.ignore
    bcast ~> trackFlow    ~> trackApi.flowCall()    ~> Sink.ignore
    SinkShape(bcast.in)
  })

  /*
   * Generate flow for exact api - filters events by type ang group by 5
   */
  private def describeApiFlow[Req, Res](implicit tag: ClassTag[Req]) = {
    Flow[RequestContainer[_, _]]
      .filter(f => filterType(f.request))
      .map(_.asInstanceOf[RequestContainer[Req, Res]])
      .grouped(groupSize)
  }

  /*
   * Generate flow for exact API - filters events by type and group into buckets by 5 or by timeout
   * Doesn't emit events if bucket is empty
   */
  private def describeApiFlowWithTimeout[Req, Res](implicit tag: ClassTag[Req]) = {
    Flow[RequestContainer[_, _]]
      .filter(f => filterType(f.request))
      .map(_.asInstanceOf[RequestContainer[Req, Res]])
      .groupedWithin(groupSize, timeout)
  }

  /* Queue to push requests for batch graph */
  private val batchSourceQueue = source.toMat(batchGraph)(Keep.left).run()

  /* Queue to push requests for batcj with timeout graph */
  private val batchWithTimeoutSourceQueue = source.toMat(batchWithTimeoutGraph)(Keep.left).run()

  /**
    * Method collects requests into batches by 5 and then makes call to corresponding API
    * @param request API request
    * @param ec execution context
    * @tparam T type of request identifier
    * @tparam R type of response from API
    * @return Future that will complete when request to API will be made
    */
  def batchedCall[T, R](request: ApiRequest[T])(implicit ec: ExecutionContext): Future[ApiResponse[R]] =
    offerToQueue(request, batchSourceQueue)

  /**
    * Method collects requests into batches by 5 or timeout and then makes call to corresponding API
    * @param request API request
    * @param ec execution context
    * @tparam T type of request identifier
    * @tparam R type of response from API
    * @return Future that will complete when request to API will be made
    */
  def batchedWithTimeoutCall[T, R](request: ApiRequest[T])(implicit ec: ExecutionContext): Future[ApiResponse[R]] =
    offerToQueue(request, batchWithTimeoutSourceQueue)

  /*
   * Push request to a queue and create empty Promise for the result of the call
   * After getting result from graph promise will be converted into Future
   */
  private def offerToQueue[R, T](call: ApiRequest[T], queue: SourceQueueWithComplete[RequestContainer[_, _]]) = {
    val promise = Promise[ApiResponse[R]]()
    queue.offer(RequestContainer[ApiRequest[T], ApiResponse[R]](call, promise)).flatMap {
      case QueueOfferResult.Enqueued => promise.future
      case QueueOfferResult.Dropped => Future.failed(new RuntimeException("Queue overflowed. Try again later."))
      case QueueOfferResult.Failure(ex) => Future.failed(ex)
      case QueueOfferResult.QueueClosed => Future.failed(new RuntimeException("Queue was closed (pool shut down) while running the request. Try again later."))
    }
  }

}