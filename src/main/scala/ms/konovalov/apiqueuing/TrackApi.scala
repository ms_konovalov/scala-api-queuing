package ms.konovalov.apiqueuing

import ms.konovalov.apiqueuing.StatusType.StatusType

import scala.concurrent.Future
import scala.util.Try

/**
  * Interface for Tracking API
  */
trait TrackApi extends Api[Int, StatusType, TrackRequest, TrackResponse]

/**
  * Simple stub implementation for Tracking API
  */
class TrackApiStub extends TrackApi {

  /**
    * @inheritdoc
    * Returns random status
    */
  override def call(numbers: Seq[Int]): Future[Seq[StatusType]] =
    Future.successful(
      numbers.map(_ => random(StatusType.allTypes))
    )

  /**
    * @inheritdoc
    * Returns random status
    */
  override def tryCall(numbers: Seq[TrackRequest]): Future[Try[Seq[TrackResponse]]] = {
    Future.successful(Try {
      numbers.map(_ => TrackResponse(random(StatusType.allTypes)))
    })
  }
}

/**
  * Enum for Status types
  */
object StatusType {

  sealed abstract class StatusType(val name: String)

  case object New extends StatusType("NEW")

  case object InTransit extends StatusType("IN TRANSIT")

  case object Collecting extends StatusType("COLLECTING")

  case object Collected extends StatusType("COLLECTED")

  case object Delivering extends StatusType("DELIVERING")

  case object Delivered extends StatusType("DELIVERED")

  val allTypes: Seq[StatusType] = Seq(New, InTransit, Collecting, Collected, Delivering, Delivered)

  def fromString(name: String): Option[StatusType] = allTypes.find(t => t.name.equals(name))
}