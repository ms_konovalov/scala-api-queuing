package ms.konovalov.apiqueuing

import akka.NotUsed
import akka.stream.scaladsl.Flow
import ms.konovalov.apiqueuing.ProductType.ProductType
import ms.konovalov.apiqueuing.StatusType.StatusType

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success, Try}

/**
  * Common interface for all provided APIs
  * @tparam ID type of identifier
  * @tparam R type of returned value
  * @tparam Req wrapped type of argument
  * @tparam Res wrapped type of returned value
  */
trait Api[ID, R, Req <: ApiRequest[ID], Res <: ApiResponse[R]] {

  /**
    * Make simple direct call
    * @param ids list of identifiers
    * @return list of values wrapped in Future
    */
  def call(ids: Seq[ID]): Future[Seq[R]]

  /**
    * Make direct API call with Try idiom
    * @param ids list of wrapped arguments
    * @return list of wrapper result wrapped with Try
    */
  def tryCall(ids: Seq[Req]): Future[Try[Seq[Res]]]

  /**
    * Create Flow that can process input arguments and emit resulting values
    * @return akka-streams Flow
    */
  def flowCall()(implicit ec: ExecutionContext): Flow[Seq[RequestContainer[Req, Res]], Seq[Promise[Res]], NotUsed] = {
    Flow[Seq[RequestContainer[Req, Res]]]
      .mapAsync(4)(request => {
        val promises = request.map(_.result)
        tryCall(request.map(_.request)).map {
          case Success(responses) =>
            responses.zip(promises).map { case (result, p) => p.success(result) }
            promises
          case Failure(e) =>
            promises.foreach(p => p.failure(e))
            promises
        }
      })
  }
}

/**
  * Wrapper for input arguments
  * @tparam ID type of identifier
  */
sealed trait ApiRequest[ID] {
  val param: ID
}

/**
  * Request for Pricing API
  * @param param country name
  */
case class PricingRequest(param: String) extends ApiRequest[String]

/**
  * Request for Shipment API
  * @param param id of shipment item
  */
case class ShipmentRequest(param: Int) extends ApiRequest[Int]

/**
  * Request for TrackAPI
  * @param param id of tracking item
  */
case class TrackRequest(param: Int) extends ApiRequest[Int]

/**
  * Wrapper for API response value
  * @tparam R type of response
  */
sealed trait ApiResponse[R] {
  val result: R
}

/**
  * Response from Shipment API
  * @param result list of Product types
  */
case class ShipmentResponse(result: Seq[ProductType]) extends ApiResponse[Seq[ProductType]]

/**
  * Response from Pricing API
  * @param result price of item
  */
case class PricingResponse(result: Float) extends ApiResponse[Float]

/**
  * Response from Tracking API
  * @param result status of item
  */
case class TrackResponse(result: StatusType) extends ApiResponse[StatusType]

/**
  * Tuple of API request and API Reponse
  * @param request API request
  * @param result API response
  * @tparam Req reques type
  * @tparam Res response type
  */
case class RequestContainer[Req, Res](request: Req, result: Promise[Res])