package ms.konovalov.apiqueuing

import scala.concurrent.Future
import scala.util.{Random, Try}

/**
  * Interface for Pricing API
  */
trait PricingApi extends Api[String, Float, PricingRequest, PricingResponse]

/**
  * Simple stub implementation for Pricing API
  */
class PricingApiStub extends PricingApi {

  /**
    * @inheritdoc
    * For each country code it generates random price from 0 to 100
    */
  def call(countries: Seq[String]): Future[Seq[Float]] = {
    Future.successful(
      countries.map(generate)
    )
  }

  /**
    * @inheritdoc
    * For each country code it generates random price from 0 to 100
    */
  def tryCall(countries: Seq[PricingRequest]): Future[Try[Seq[PricingResponse]]] = {
    Future.successful(Try {
      countries.map(t => PricingResponse(generate(t.param)))
    })
  }

  private def generate(country: String) = {
    new Random().nextFloat() * 100
  }
}
