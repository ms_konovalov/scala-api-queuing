package ms.konovalov.apiqueuing

import ms.konovalov.apiqueuing.ProductType.ProductType

import scala.concurrent.Future
import scala.util.Try

/**
  * Interface for Shipment API
  */
trait ShipmentApi extends Api[Int, Seq[ProductType], ShipmentRequest, ShipmentResponse]

/**
  * Simple stub implementation for Pricing API
  */
class ShipmentApiStub extends ShipmentApi {

  /**
    * @inheritdoc
    * Returns list of Products with size equal to last digit in identifier
    */
  def call(numbers: Seq[Int]): Future[Seq[Seq[ProductType]]] = {
    Future.successful(
      numbers.map(number => generateProducts(number))
    )
  }

  /**
    * @inheritdoc
    * Returns list of Products with size equal to last digit in identifier
    */
  def tryCall(numbers: Seq[ShipmentRequest]): Future[Try[Seq[ShipmentResponse]]] = {
    Future.successful(Try {
      numbers.map(number => ShipmentResponse(generateProducts(number.param)))
    })
  }

  private def generateProducts(number: Int): Seq[ProductType] = {
    (1 to (number % 10)).map(_ => random(ProductType.allTypes))
  }
}

/**
  * Enum of product types
  */
object ProductType {

  sealed abstract class ProductType(val name: String)

  case object Envelope extends ProductType("envelope")

  case object Box extends ProductType("box")

  case object Pallet extends ProductType("pallet")

  val allTypes = Seq(Envelope, Box, Pallet)

  def fromString(name: String): Option[ProductType] = allTypes.find(t => t.name.equals(name))
}