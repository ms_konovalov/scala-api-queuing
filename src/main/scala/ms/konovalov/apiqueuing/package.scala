package ms.konovalov

import akka.http.scaladsl.model.{HttpResponse, ResponseEntity, StatusCodes}
import akka.http.scaladsl.unmarshalling.{Unmarshal, Unmarshaller}
import akka.stream.Materializer

import scala.concurrent.{ExecutionContext, Future}
import scala.reflect.ClassTag
import scala.util.{Failure, Try}

package object apiqueuing {

  /**
    * Method that takes random item from list
    *
    * @param s list of items
    * @tparam T type of items
    * @return random item
    */
  def random[T](s: Seq[T]): T = {
    val n = util.Random.nextInt(s.size)
    s.iterator.drop(n).next
  }

  /**
    * Check whether object is of provided type
    * @param obj pbject to test
    * @param tag class tag of checked type
    * @tparam T checked type
    * @return true if object is instance of type
    */
  def filterType[T](obj: Any)(implicit tag: ClassTag[T]): Boolean =
    obj match {
      case _: T => true
      case _ => false
    }

  /**
    *  Deserialize http response
    * @param response http response
    * @tparam T result type
    * @return deserialized value wrapped in Try and Future
    */
  def deserialize[T](response: HttpResponse)
                    (implicit tag: ClassTag[T], um: Unmarshaller[ResponseEntity, T], mat: Materializer, ec: ExecutionContext): Future[Try[T]] =
    response.status match {
      case StatusCodes.OK => Unmarshal(response.entity).to[T].map(Try.apply(_))
      case _ => Future.successful(Failure(UnexpectedStatusCode(response.status)))
    }
}
