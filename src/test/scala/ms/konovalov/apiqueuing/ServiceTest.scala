package ms.konovalov.apiqueuing

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.testkit.TestKit
import com.typesafe.config.ConfigFactory
import org.scalatest._

class ServiceTest(_system: ActorSystem) extends TestKit(_system) with AsyncFlatSpecLike
  with Matchers with BeforeAndAfterAll with BeforeAndAfterEach {

  def this() = this(ActorSystem("MySpec", ConfigFactory.parseString(
    """akka {
    |  loggers = ["akka.event.slf4j.Slf4jLogger"]
    |  loglevel = "DEBUG"
    |  logging-filter = "akka.event.slf4j.Slf4jLoggingFilter"
    }""".stripMargin)))

  implicit val mat = ActorMaterializer()
  private val service = new Service()

  "Service" should "return empty list" in {
    service.directCall(Seq.empty) map (_ shouldBe empty)
  }

  "Service" should "return result" in {
    val request = Seq(PricingRequest("NL"), PricingRequest("GB"))
    val eventualSeq = service.directCall(request)
    eventualSeq map (_ should not be empty)
    eventualSeq map (_ should have size 2)
  }

  "Service" should "make batch call for 1 api" in {
    val future1 = service.batchedCall(ShipmentRequest(123))
    val future2 = service.batchedCall(ShipmentRequest(121))
    val future3 = service.batchedCall(ShipmentRequest(122))
    val future4 = service.batchedCall(ShipmentRequest(120))
    val future5 = service.batchedCall(PricingRequest("NL"))

    future1.isCompleted shouldBe false
    future2.isCompleted shouldBe false
    future3.isCompleted shouldBe false
    future4.isCompleted shouldBe false
    future5.isCompleted shouldBe false
    val future6 = service.batchedCall(ShipmentRequest(321))

    future6 map (f => {
      f shouldBe a[ApiResponse[_]]
      future1.isCompleted shouldBe true
      future2.isCompleted shouldBe true
      future3.isCompleted shouldBe true
      future4.isCompleted shouldBe true
      future5.isCompleted shouldBe false

      f.asInstanceOf[ShipmentResponse].result should have size 1
    })
  }

  "Service" should "make call after 1 sec" in {
    val future = service.batchedWithTimeoutCall(PricingRequest("NL"))
    future.map (_.asInstanceOf[PricingResponse].result shouldBe a[java.lang.Float])
  }

  "Service" should "not make call" in {
    val future = service.batchedCall(PricingRequest("NL"))

    val f = service.batchedWithTimeoutCall(PricingRequest("NL"))

    f.map {
      future.isCompleted shouldBe false
      _.asInstanceOf[PricingResponse].result shouldBe a[java.lang.Float]}
  }


}
