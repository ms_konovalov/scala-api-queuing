import sbt._

object Dependencies {

  val main = Seq(
    "com.typesafe.akka" %% "akka-http-core" % "10.0.5",
    "com.typesafe.akka" %% "akka-http-testkit" % "10.0.5",
    "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.5",
    "org.scalatest" %% "scalatest" % "3.0.1" % "test",
    "org.scalamock" %% "scalamock-scalatest-support" % "3.5.0" % "test",
  "ch.qos.logback" % "logback-classic" % "1.1.3",
  "com.typesafe.akka" %% "akka-slf4j" % "2.4.17"
  )
}